### About Okapi Olifant

**Okapi Olifant** is a cross-platform application to manage translation memories.  
It allows you to create, modify, import, export and use translation memories and work with files such as TMX, PO, XLIFF, and other translation formats.

Please note this project is still **ALPHA** and to use for **test only**.  
For production you can still use the older version of [Olifant for .NET](http://okapi.sourceforge.net/downloads.html).

### Build status

`dev` branch: [![pipeline status](https://gitlab.com/okapiframework/olifant/badges/dev/pipeline.svg)](https://gitlab.com/okapiframework/olifant/commits/dev)

`master` branch: [![pipeline status](https://gitlab.com/okapiframework/olifant/badges/master/pipeline.svg)](https://gitlab.com/okapiframework/olifant/commits/master)

### Downloads

The snapshots (development versions) of Olifant can be downloaded from https://gitlab.com/okapiframework/olifant/-/jobs/artifacts/dev/browse/deployment/maven/done?job=verification
