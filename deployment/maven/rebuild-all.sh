#!/bin/bash -e

cd ../..
mvn clean
mvn install

mvn dependency:resolve -f applications/olifant/pom.xml -PWIN_64_SWT -PCOCOA_64_SWT -PLinux_x86_64_swt

cd deployment/maven
ant

chmod a+x dist_gtk2-linux-x86_64/olifant.sh
